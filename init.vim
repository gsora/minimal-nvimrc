if &compatible
 set nocompatible
endif
" Add the dein installation directory into runtimepath
set runtimepath+=~/.cache/dein/repos/github.com/Shougo/dein.vim

if dein#load_state('~/.cache/dein')
 call dein#begin('~/.cache/dein')

 call dein#add('~/.cache/dein')
 call dein#add('itchyny/lightline.vim')
 call dein#add('fatih/vim-go')
 call dein#add('zchee/deoplete-go', {'build': 'make'})
 call dein#add('rust-lang/rust.vim')
 call dein#add('Shougo/echodoc.vim')
 call dein#add('andreasvc/vim-256noir')
 call dein#add('reedes/vim-pencil')
 call dein#add('junegunn/goyo.vim')
 call dein#add('Shougo/deoplete.nvim')
 call dein#add('junegunn/goyo.vim')
 call dein#add('junegunn/limelight.vim')
 call dein#add('junegunn/seoul256.vim')

 if !has('nvim')
   call dein#add('roxma/nvim-yarp')
   call dein#add('roxma/vim-hug-neovim-rpc')
 endif

 call dein#end()
 call dein#save_state()
endif

filetype plugin indent on
syntax enable

set number
set mouse=a

colorscheme seoul256
let g:deoplete#enable_at_startup = 1

set noshowmode
let g:echodoc_enable_at_startup = 1
let g:rustfmt_autosave = 1

let g:pencil#wrapModeDefault = 'soft'   " default is 'hard'
augroup pencil
  autocmd!
  autocmd FileType markdown,mkd call pencil#init()
  autocmd FileType text         call pencil#init({'wrap': 'hard'})
augroup END

function! s:goyo_enter()
  set noshowcmd
  set scrolloff=999
  Limelight
  let g:deoplete#disable_auto_complete = 1
endfunction

function! s:goyo_leave()
  set showcmd
  set scrolloff=5
  Limelight!
  let g:deoplete#disable_auto_complete = 0
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()
let g:markdown_enable_spell_checking = 0

let g:go_fmt_command = "goimports"

let g:lightline = {
      \ 'colorscheme': 'seoul256',
      \ }
